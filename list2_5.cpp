#include <iostream>
#include <iomanip>
using namespace std;

int main(){
    double height;
    double weight;
    double bmi;

    cout << "身長(m)を入力してください:";
    cin >> height;

    cout << "体重(kg)を入力してください:";
    cin >> weight;

    bmi = weight / height / height;

    cout << "あなたのBMIは、" << fixed << setprecision(1) << bmi << "です。" << endl;  //四捨五入

    return 0;
}